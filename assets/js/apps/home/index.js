"use strict";

!function (NioApp, $) {
  "use strict";

  $("#btn-monitor-device").on("click",function() {
    $("#btn-monitor-device").attr("disabled", true);
    $("#btn-monitor-device").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);
  });

  // Load Total Meta
  loadMetaTotal()

  // Load Last Activity
  loadLastActivity()

}(NioApp, jQuery);

function loadMetaTotal() {

  $("#meta-total-data").html(`
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  <span> Loading... </span>
  `);

  global.getRAW(global.base_url + "/services/meta/home?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
          $("#meta-total-data").html(`
          <div class="col-md-4">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group align-start mb-0">
                        <div class="card-title">
                            <h6 class="subtitle">Total Perangkat</h6>
                        </div>
                        <div class="card-tools">
                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                        </div>
                    </div>
                    <div class="card-amount">
                        <span class="amount">${res.data.devices.total}
                        </span>
                        <span class="change up text-danger">
                            <em class="icon ni ni-archive"></em> Perangkat
                        </span>
                    </div>
                    <div class="invest-data">
                        <div class="invest-data-amount g-2">
                            <div class="invest-data-history">
                                <div class="title">Bulan Ini</div>
                                <div class="amount">${res.data.devices.month}</div>
                            </div>
                            <div class="invest-data-history">
                                <div class="title">Hari Ini</div>
                                <div class="amount">${res.data.devices.day}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .card -->
        </div>

        <div class="col-md-4">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group align-start mb-0">
                        <div class="card-title">
                            <h6 class="subtitle">Total Pengguna</h6>
                        </div>
                        <div class="card-tools">
                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Pengguna"></em>
                        </div>
                    </div>
                    <div class="card-amount">
                        <span class="amount">${res.data.users.total}
                        </span>
                        <span class="change up text-danger">
                            <em class="icon ni ni-users"></em> Pengguna
                        </span>
                    </div>
                    <div class="invest-data">
                        <div class="invest-data-amount g-2">
                            <div class="invest-data-history">
                                <div class="title">Bulan Ini</div>
                                <div class="amount">${res.data.users.month}</div>
                            </div>
                            <div class="invest-data-history">
                                <div class="title">Hari Ini</div>
                                <div class="amount">${res.data.users.day}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .card -->
        </div>

        <div class="col-md-4">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group align-start mb-0">
                        <div class="card-title">
                            <h6 class="subtitle">Tiket Yang Masuk</h6>
                        </div>
                        <div class="card-tools">
                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Perangkat Yang Aktif"></em>
                        </div>
                    </div>
                    <div class="card-amount">
                        <span class="amount">${res.data.ticketing.total}
                        </span>
                        <span class="change up text-danger">
                            <em class="icon ni ni-archive"></em> Perangkat
                        </span>
                    </div>
                    <div class="invest-data">
                        <div class="invest-data-amount g-2">
                            <div class="invest-data-history">
                                <div class="title">Bulan Ini</div>
                                <div class="amount">${res.data.ticketing.month}</div>
                            </div>
                            <div class="invest-data-history">
                                <div class="title">Hari Ini</div>
                                <div class="amount">${res.data.ticketing.day}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .card -->
        </div>
          `);
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadLastActivity() {
  $("#log-last").html(`
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  <span> Loading... </span>
  `);

  global.getRAW(global.base_url + "/services/log/index?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
          $("#log-last").html(`
            ${res.data.map(function(item) {
                return `
                <li class="nk-activity-item">
                    <div class="nk-activity-data">
                        <div class="label">${item.ip_address} | ${item.title}</div>
                        <div class="time">${item.description}</div>
                        <span class="time">${global.date(item.created_at)}</span>
                    </div>
                </li>
                `;
            }).join('')}
          `);
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}