"use strict";

console.log("General JS");

function showTogglePassword() {
    var x = document.getElementById("password");
    if (x.type === "password") x.type = "text";
    else x.type = "password";
}

function showToggleDisabled(id) {
    var x = document.getElementById(id);
    if (x.disabled) x.disabled = false;
    else x.disabled = true;
}

var global = {
    base_url: $("#base_url").val(),
    csrf_token: $("#csrf_token").val(),
    data: function(response) {
        return JSON.parse(response)
    },
    raw: function(obj) {
        return JSON.stringify(obj)
    },
    money: function(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
    },
    date: function(date="") {
        var dateSplit = date.split("-");
        var month = dateSplit[1];
        var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", 
        "Agustus", "September", "Oktober", "November", "Desember"];
        month = bulan[Number(month) - 1];
        dateSplit[1] = month;
        dateSplit = dateSplit.join(" ");
        dateSplit = dateSplit.split(" ");
        return dateSplit[2] + " " + dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[3];
    },
    capitalize: function(string) {
        var splitStr = string.split(' ')
        var fullStr = '';
    
        $.each(splitStr,function(index){
            var currentSplit = splitStr[index].charAt(0).toUpperCase() + splitStr[index].slice(1);
            fullStr += currentSplit + " "
        });
    
        return fullStr;
    },
    random: function (min, max, step) {
        const randomNum = min + Math.random() * (max - min);
        return Math.round(randomNum / step) * step;
    },
    currDate: function() {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        month = (month < 10) ? "0" + month : "" + month;
        day = (day < 10) ? "0" + day : "" + day;

        return year + "-" + month + "-" + day;
    },
    currTime: function() {
        var today = new Date();
        return today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    },
    convTime: function(time) {
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if(AMPM == "PM" && hours<12) hours = hours+12;
        if(AMPM == "AM" && hours==12) hours = hours-12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if(hours<10) sHours = "0" + sHours;
        if(minutes<10) sMinutes = "0" + sMinutes;

        return sHours + ":" + sMinutes + ":59"
    },
    month: function(date) {
        var splitDate = date.split("-");
        var dataMonth = splitDate[1];
        switch (dataMonth) {
            case "01":
            case "1": return "Jan";
            case "02":
            case "2": return "Feb";
            case "03":
            case "3": return "Mar";
            case "04":
            case "4": return "Apr";
            case "05":
            case "5": return "May";
            case "06":
            case "6": return "Jun";
            case "07":
            case "7": return "Jul";
            case "08":
            case "8": return "Aug";
            case "09":
            case "9": return "Sep";
            case "10":
            case "10": return "Oct";
            case "11":
            case "11": return "Nov";
            default: return "Dec";
        }
    }
}

// ============ GENERAL

// POST
global.postRAW = function(req, url, callback) {
    $.ajax({
        url: url,
        data: req,
        type: "POST",
        contentType: false,
        processData: false,
        success: function(response) {
            response = global.data(response)
            callback(response);
        }
    });
}

// GET
global.getRAW = function(url, callback) {
    $.ajax({
        url: url,
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = global.data(response)
            callback(response);
        }
    });
}