"use strict";

!function (NioApp, $) {
  "use strict";

  $("#btn-device-back").on("click",function() {
    $("#btn-device-back").attr("disabled", true);
    $("#btn-device-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
      window.history.back();
    }, 1000);
  });

  $("#btn-device-new").on("click",function() {
    $("#btn-device-new").attr("disabled", true);
    $("#btn-device-new").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doAddDevice();
  });
}(NioApp, jQuery);

function doAddDevice() {
  var device_name = $("#device-new-name").val()
  var device_type = $("#device-new-type").val()

  var raw = global.raw({
    device_name: device_name,
    device_type: device_type
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/devices/create?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-device-new").removeAttr("disabled");
      $("#btn-device-new").html(`
        <span>Simpan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#device-new-form").trigger("reset");

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}