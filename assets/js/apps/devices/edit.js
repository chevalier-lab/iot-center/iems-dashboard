"use strict";

!function (NioApp, $) {
  "use strict";

  $('#user-new-phone_number').mask('(999) 999-999-999?9');

  $("#btn-user-back").on("click",function() {
    $("#btn-user-back").attr("disabled", true);
    $("#btn-user-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
        location.assign(base_url + "/views/users");
    }, 1000);
  });
}(NioApp, jQuery);