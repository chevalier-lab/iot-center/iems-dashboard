"use strict";

var devices = [];
var device = null;

!function (NioApp, $) {
  "use strict";

  loadMetaTotal()

  filterDevices("");

  $("#btn-device-add").on("click",function() {
    $("#btn-device-add").attr("disabled", true);
    $("#btn-device-add").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
        location.assign(global.base_url + "/views/devicesAdd");
    }, 1000);
  });

  $("#btn-device-activate").on("click",function() {
    $("#btn-device-activate").attr("disabled", true);
    $("#btn-device-activate").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doActivateDevice();
  });
}(NioApp, jQuery);

function loadMetaTotal()
{
  $("#meta-total-data").html(`
    <center>
      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
      <span> Loading... </span>
    </center>
  `);

  global.getRAW(global.base_url + "/services/meta/devices?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
          $("#meta-total-data").html(`
            <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">PCB</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="PCB"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.pcb}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-archive"></em> Devices
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Sensors</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Sensors"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.sensors}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-archive"></em> Devices
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">SLCA</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="SLCA"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.slca}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-archive"></em> Devices
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">KWH 1 Phase</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="KWH 1 Phase"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.kwh_1_phase}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-archive"></em> Devices
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">KWH 3 Phase</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="KWH 3 Phase"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.kwh_3_phase}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-archive"></em> Devices
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>
          `);
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function gotoDetailDevice(position) { 
    device = devices[position];
    location.assign(global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + device.device_token + "&uname=" + device.device_name + "&utype=" + device.device_type);
}

function setActivate(position) {
    device = devices[position];
    $("#modal-activate-device-name").text(device.device_name)
    $("#device-activate-name").val(device.device_name)
}

function doActivateDevice() {
var phone_number = $("#device-activate-phone_number").val();
  var device_token = device.device_token;
  var device_name = $("#device-activate-name").val()

  var raw = global.raw({
    phone_number: phone_number,
    device_token: device_token,
    device_name: device_name
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/devices/activate?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-device-activate").removeAttr("disabled");
      $("#btn-device-activate").html(`
        <span>Aktifkan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#modal-activate-device-form").trigger("reset");
        $("#modal-activate-device").modal('toggle');
        filterDevices("");
        loadMetaTotal()

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function filterDevices(type)
{
    $("#list-devices tbody").html(`
        <tr>
            <td scope="col" colspan="6">
                <center>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span> Loading... </span>
                </center>
            </td>
        </tr>
    `);

    global.getRAW(global.base_url + "/services/devices/filter?type=" + type + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            devices = res.data;
            $("#list-devices tbody").html(`${devices.map(function(item, position) {
                var type = getStatus(item.device_type);
                var status = getStatus(item.device_status);
                var action = `
                <button class="btn btn-icon btn-sm btn-primary mb-1"
                onclick="gotoDetailDevice(${position})">
                <em class="icon ni ni-eye"></em></button>
                `;

                if (item.device_status == "not active yet") {
                  action += `
                  <button class="btn btn-icon btn-sm btn-success mb-1"
                  data-toggle="modal" data-target="#modal-activate-device"
                  onclick="setActivate(${position})">
                  <em class="icon ni ni-check"></em></button>
                  
                  <button class="btn btn-icon btn-sm btn-danger mb-1"
                  data-toggle="modal" data-target="#modalDelete">
                  <em class="icon ni ni-trash"></em></button>`
                } else if (item.device_status == "blocked") {
                  action += `
                  <button class="btn btn-icon btn-sm btn-success mb-1"
                  data-toggle="modal" data-target="#modalEnabled">
                  <em class="icon ni ni-check"></em></button>`
                } else {
                  action += `
                  <button class="btn btn-icon btn-sm btn-danger mb-1"
                  data-toggle="modal" data-target="#modalBanned">
                  <em class="icon ni ni-cross"></em></button>`
                }

                return `
                <tr>
                  <th scope="row">${(position + 1)}</th>
                  <td>
                      ${item.device_name}<br>
                      <small>${item.device_shortname}</small>
                  </td>
                  <td>
                      <small>${item.device_token}</small>
                  </td>
                  <td>${type}</td>
                  <td>${status}</td>
                  <td>${(item.device_status == "not active yet") ? "-" : global.date(item.updated_at)}</td>
                  <td>${action}</td>
              </tr>
                `;
            }).join('')}`);
            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getStatus(status) {
    var newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
    switch (status) {
        case "kwh-1-phase": newStatus = `<span class="badge badge-pill badge-outline-warning">KWH 1 Fasa</span>`;
        break;
        case "kwh-3-phase": newStatus = `<span class="badge badge-pill badge-outline-info">KWH 3 Fasa</span>`;
        break;
        case "pcb": newStatus = `<span class="badge badge-pill badge-outline-primary">PCB</span>`;
        break;
        case "sensors": newStatus = `<span class="badge badge-pill badge-outline-success">Sensor</span>`;
        break;
        case "slca": newStatus = `<span class="badge badge-pill badge-outline-secondary">SLCA</span>`;
        break;
        case "active": newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
        break;
        case "not active yet": newStatus = `<span class="badge badge-pill badge-secondary">Belum Diaktifasi</span>`
        break;
        case "blocked": newStatus = `<span class="badge badge-pill badge-danger">Terblokir</span>`
        break;
    }
    return newStatus;
}