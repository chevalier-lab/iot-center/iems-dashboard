"use strict";

var tickets = []

!function (NioApp, $) {
  "use strict";

  // Load Meta Total
  loadMetaTotal()

  // Load Filter Users
  filterTickets(""); // Load All

}(NioApp, jQuery);

// function gotoEditUser() { location.assign(base_url + "/views/usersEdit/1"); }

function loadMetaTotal()
{
  $("#meta-total-data").html(`
    <center>
      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
      <span> Loading... </span>
    </center>
  `);

  global.getRAW(global.base_url + "/services/meta/tickets?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
          $("#meta-total-data").html(`
            <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Tiket Baru</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tiket Baru"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.new}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-ticket-alt"></em> Tiket
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Tiket Terproses</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tiket Terproses"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.process}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-ticket-alt"></em> Tiket
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Tiket Selesai</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tiket Selesai"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.finish}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-ticket-alt"></em> Tiket
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Tiket Ditolak</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tiket Ditolak"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.rejected}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-ticket-alt"></em> Tiket
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>
          `);
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function gotoDetailUser(position) {
    var user = users[position];
    location.assign(global.base_url + "/views/usersDetail?tokenize=" + global.csrf_token + "&uid=" + user.phone_number + "&uname=" + user.full_name);
}

function filterTickets(type)
{
    $("#ticket-list tbody").html(`
        <tr>
            <td scope="col" colspan="6">
                <center>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span> Loading... </span>
                </center>
            </td>
        </tr>
    `);

    global.getRAW(global.base_url + "/services/ticketing/filter?type=" + type + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            tickets = res.data;
            $("#ticket-list tbody").html(`${tickets.map(function(item, position) {
                var status = getStatus(item.status);
                return `
                <tr>
                    <th scope="row">${(position + 1)}</th>
                    <td>${item.full_name}</td>
                    <td>${item.phone_number}</td>
                    <td>${item.title}</td>
                    <td>${status}</td>
                    <td>${global.date(item.updated_at)}</td>
                    <td>
                        <button class="btn btn-icon btn-sm btn-primary">
                        <em class="icon ni ni-eye"></em></button>
                    </td>
                </tr>
                `;
            }).join('')}`);
            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getStatus(status) {
    var newStatus = `<span class="badge badge-pill badge-info">Baru</span>`
    switch (status) {
        case "new": newStatus = `<span class="badge badge-pill badge-info">Baru</span>`;
        break;
        case "process": newStatus = `<span class="badge badge-pill badge-warning">Diproses</span>`;
        break;
        case "finish": newStatus = `<span class="badge badge-pill badge-success">Selesai</span>`;
        break;
        case "rejected": newStatus = `<span class="badge badge-pill badge-danger">Ditolak</span>`;
        break;
    }
    return newStatus;
}