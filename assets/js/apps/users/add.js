"use strict";

!function (NioApp, $) {
  "use strict";

  $('#user-new-phone_number').mask('(999) 999-999-999?9');

  checkIsUser("guest");

  $("#btn-user-back").on("click",function() {
    $("#btn-user-back").attr("disabled", true);
    $("#btn-user-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
      window.history.back();
    }, 1000);
  });

  $("#btn-user-new").on("click",function() {
    $("#btn-user-new").attr("disabled", true);
    $("#btn-user-new").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateUser();
  });

}(NioApp, jQuery);

function checkIsUser(type) {
  if (type == "user") $(".if-user").show();
  else $(".if-user").hide();
}

function doCreateUser() {
  var full_name = $("#user-new-full_name").val()
  var email = $("#user-new-email").val()
  var password = $("#user-new-password").val()
  var phone_number = $("#user-new-phone_number").val()
  var type = $("#user-new-type").val()
  var ssid_name = $("#user-new-ssid_name").val()
  var ssid_password = $("#user-new-ssid_password").val()
  var status = (type == "guest") ? "not activate yet" : "activate"

  phone_number = phone_number.replace("-", "");
  phone_number = phone_number.replace("-", "");
  phone_number = phone_number.replace("(", "");
  phone_number = phone_number.replace(")", "");
  phone_number = phone_number.replace(" ", "");

  var raw = global.raw({
    full_name: full_name,
    email: email,
    password: password,
    phone_number: phone_number,
    type: type,
    status: status,
    ssid_name: ssid_name,
    ssid_password: ssid_password
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/users/create?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-user-new").removeAttr("disabled");
      $("#btn-user-new").html(`
        <span>Simpan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#user-new-form").trigger("reset");

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}