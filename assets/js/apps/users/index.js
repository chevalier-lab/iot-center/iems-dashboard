"use strict";

var users = []
var operators = [];
var user = null;
var selectedType = "guest";

!function (NioApp, $) {
  "use strict";

  $("#btn-user-add").on("click",function() {
    $("#btn-user-add").attr("disabled", true);
    $("#btn-user-add").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
        location.assign(global.base_url + "/views/usersAdd");
    }, 1000);
  });

  $("#btn-modal-delete-user").on("click",function() {
    $("#btn-modal-delete-user").attr("disabled", true);
    $("#btn-modal-delete-user").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doDeleteUser();
  });

  $("#btn-user-banned").on("click",function() {
    $("#btn-user-banned").attr("disabled", true);
    $("#btn-user-banned").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doBannedUser();
  });

  $("#btn-user-activate").on("click",function() {
    $("#btn-user-activate").attr("disabled", true);
    $("#btn-user-activate").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doActivateUser();
  });

  $("#btn-user-instalation").on("click",function() {
    $("#btn-user-instalation").attr("disabled", true);
    $("#btn-user-instalation").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doInstalationUser();
  });

  $("#btn-user-plot").on("click",function() {
    $("#btn-user-plot").attr("disabled", true);
    $("#btn-user-plot").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doPlotUser();
  });

  // Load Meta Total
  loadMetaTotal()

  // Load Filter Users
  filterUsers("guest")

}(NioApp, jQuery);

function gotoEditUser() { location.assign(base_url + "/views/usersEdit/1"); }

function loadMetaTotal()
{
  $("#meta-total-data").html(`
    <center>
      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
      <span> Loading... </span>
    </center>
  `);

  global.getRAW(global.base_url + "/services/meta/users?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
          $("#meta-total-data").html(`
            <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Pengguna Biasa</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Pengguna Biasa"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.guest}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Permintaan Berlangganan</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Permintaan Berlangganan"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.guest_subscribe}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Pengguna Terverifikasi</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Pengguna Terverifikasi"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.guest_verify}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Pengguna Aktif (Pembeli)</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Pengguna Aktif (Pembeli)"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.user_active}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Pengguna Tidak Aktif (Pembeli)</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Pengguna Tidak Aktif (Pembeli)"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.user_banned}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Administrator</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Administrator"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.admin}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">Operator</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Operator"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.operator}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>

          <div class="col-md-4 my-1">
              <div class="card card-bordered card-full">
                  <div class="card-inner">
                      <div class="card-title-group align-start mb-0">
                          <div class="card-title">
                              <h6 class="subtitle">CS</h6>
                          </div>
                          <div class="card-tools">
                              <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="CS"></em>
                          </div>
                      </div>
                      <div class="card-amount">
                          <span class="amount">${res.data.cs}
                          </span>
                          <span class="change up text-danger">
                              <em class="icon ni ni-users"></em> Pengguna
                          </span>
                      </div>
                  </div>
              </div><!-- .card -->
          </div>
          `);
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function gotoDetailUser(position) {
    user = users[position];
    location.assign(global.base_url + "/views/usersDetail?tokenize=" + global.csrf_token + "&uid=" + user.phone_number + "&uname=" + user.full_name);
}

function gotoEditUser(position) {
    user = users[position];
    location.assign(global.base_url + "/views/usersEdit?tokenize=" + global.csrf_token + "&uid=" + user.phone_number + "&uname=" + user.full_name);
}

function setPlotUser(position) {
    user = users[position];
    $("#modal-plot-user-full_name").text(user.full_name);

    loadOperator();
}

function setRemoveUser(position) {
    user = users[position];
    $("#modal-delete-user-full_name").text(user.full_name);
}

function setBannedUser(position) {
    user = users[position];
    $("#modal-blocked-user-full_name").text(user.full_name);
}

function setActivateUser(position) {
    user = users[position];
    $("#modal-activate-user-full_name").text(user.full_name);
}

function setInstalationUser(position) {
    user = users[position];
    $("#modal-instalation-user-full_name").text(user.full_name);
}

function doDeleteUser() {
    global.getRAW(global.base_url + "/services/users/delete?phone_number=" + user.phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {
        $("#btn-modal-delete-user").removeAttr("disabled");
        $("#btn-modal-delete-user").html(`
            <span>Hapus</span>
            <em class="icon ni ni-arrow-right"></em>
        `);

        if (res.code == 200) {
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            $("#modal-delete-user").modal('toggle');
            filterUsers(selectedType);
            loadMetaTotal()
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function doBannedUser() {
    global.getRAW(global.base_url + "/services/users/banned?phone_number=" + user.phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {
        $("#btn-user-banned").removeAttr("disabled");
        $("#btn-user-banned").html(`
            <span>Hapus</span>
            <em class="icon ni ni-arrow-right"></em>
        `);

        if (res.code == 200) {
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            $("#modal-blocked-user").modal('toggle');
            filterUsers(selectedType);
            loadMetaTotal()
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function doActivateUser() {
    global.getRAW(global.base_url + "/services/users/activate?phone_number=" + user.phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {
        $("#btn-user-activate").removeAttr("disabled");
        $("#btn-user-activate").html(`
            <span>Aktifkan</span>
            <em class="icon ni ni-arrow-right"></em>
        `);

        if (res.code == 200) {
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            $("#modal-activate-user").modal('toggle');
            filterUsers(selectedType);
            loadMetaTotal()
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function doInstalationUser() {
  var phone_number = user.phone_number;
  var ssid_name = $("#user-new-ssid_name").val()
  var ssid_password = $("#user-new-ssid_password").val()

  var raw = global.raw({
    phone_number: phone_number,
    ssid_name: ssid_name,
    ssid_password: ssid_password
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/users/instalation?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-user-instalation").removeAttr("disabled");
      $("#btn-user-instalation").html(`
        <span>Simpan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#modal-instalation-user-form").trigger("reset");
        $("#modal-instalation-user").modal('toggle');
        filterUsers(selectedType);
        loadMetaTotal()

        setTimeout(() => {
            location.assign(global.base_url + "/views/usersDetail?tokenize=" + global.csrf_token + "&uid=" + user.phone_number + "&uname=" + user.full_name)
        }, 1000);

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function doPlotUser() {
    var phone_number_operator = $("#user-plot-operator").val();

    if (phone_number_operator == "") {
        $("#btn-user-plot").removeAttr("disabled");
        $("#btn-user-plot").html(`
            <span>Plot</span>
            <em class="icon ni ni-arrow-right"></em>
        `);
        NioApp.Toast("Harap memilih operator", 'error', {position: 'bottom-right'});
        return 
    }

    global.getRAW(global.base_url + "/services/users/plotOperator?phone_number=" + user.phone_number + "&phone_number_operator=" + phone_number_operator + "&tokenize=" + global.csrf_token, 
    function(res) {
        $("#btn-user-plot").removeAttr("disabled");
        $("#btn-user-plot").html(`
            <span>Plot</span>
            <em class="icon ni ni-arrow-right"></em>
        `);

        if (res.code == 200) {
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            $("#modal-plot-user-form").trigger("reset");
            $("#modal-plot-user").modal('toggle');
            filterUsers(selectedType);
            loadMetaTotal()
            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadOperator()
{
    global.getRAW(global.base_url + "/services/users/filter?type=operator&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            operators = res.data;

            $("#user-plot-operator").html(`
            <option value="">Pilih Operator</option>
            ${operators.map(function(item) {
                return `
                    <option value="${item.phone_number}">${item.full_name}</option>
                `;
            }).join('')}`);
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function filterUsers(type)
{
    $("#user-list tbody").html(`
        <tr>
            <td scope="col" colspan="6">
                <center>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span> Loading... </span>
                </center>
            </td>
        </tr>
    `);

    type = type.replace(" ", "-");
    selectedType = type;

    global.getRAW(global.base_url + "/services/users/filter?type=" + type + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            users = res.data;

            if (users.length == 0) {
                $("#user-list tbody").html(`
                    
                    <tr>
                        <td colspan="6">
                            <center>Belum ada pengguna</center>
                        </td>
                    </tr>
                        
                `);
                return
            }

            $("#user-list tbody").html(`
                ${res.data.map(function(item, position) {
                    var status = ``;
                    var action = ``;

                    if (item.type == "administrator") {
                        status = `<span class="badge badge-pill badge-primary">Administrator</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-warning"
                        onclick="gotoEditUser(${position})">
                        <em class="icon ni ni-edit"></em></button>
                        `;
                    }
                    else if (item.type == "operator") {
                        status = `<span class="badge badge-pill badge-primary">Operator</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-warning"
                        onclick="gotoEditUser(${position})">
                        <em class="icon ni ni-edit"></em></button>
                        `;

                        if (item.status == "activate") {
                            action += `<button class="btn btn-icon btn-sm btn-danger"
                            data-toggle="modal" data-target="#modal-blocked-user"
                            onclick="setBannedUser(${position})">
                            <em class="icon ni ni-cross"></em></button>
                            `;
                        } else if (item.status == "banned") {
                            status = `<span class="badge badge-pill badge-danger">Tidak Aktif (Operator)</span>`;
                            action += `
                            <button class="btn btn-icon btn-sm btn-success"
                            data-toggle="modal" data-target="#modal-activate-user"
                            onclick="setActivateUser(${position})">
                            <em class="icon ni ni-check"></em></button>
                            `;
                        }
                    }
                    else if (item.type == "customer service") {
                        status = `<span class="badge badge-pill badge-primary">CS</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-warning"
                        onclick="gotoEditUser(${position})">
                        <em class="icon ni ni-edit"></em></button>
                        `;

                        if (item.status == "activate") {
                            action += `<button class="btn btn-icon btn-sm btn-danger"
                            data-toggle="modal" data-target="#modal-blocked-user"
                            onclick="setBannedUser(${position})">
                            <em class="icon ni ni-cross"></em></button>
                            `;
                        } else if (item.status == "banned") {
                            status = `<span class="badge badge-pill badge-danger">Tidak Aktif (CS)</span>`;
                            action += `
                            <button class="btn btn-icon btn-sm btn-success"
                            data-toggle="modal" data-target="#modal-activate-user"
                            onclick="setActivateUser(${position})">
                            <em class="icon ni ni-check"></em></button>
                            `;
                        }
                    }
                    else if (item.type == "user") {
                        status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-warning"
                        onclick="gotoEditUser(${position})">
                        <em class="icon ni ni-edit"></em></button>
                        `;

                        if (item.status == "activate") {
                            status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
                            action += `<button class="btn btn-icon btn-sm btn-danger"
                            data-toggle="modal" data-target="#modal-blocked-user"
                            onclick="setBannedUser(${position})">
                            <em class="icon ni ni-cross"></em></button>
                            `;
                        } else if (item.status == "banned") {
                            status = `<span class="badge badge-pill badge-danger">Tidak Aktif (Pembeli)</span>`;
                            action += `
                            <button class="btn btn-icon btn-sm btn-success"
                            data-toggle="modal" data-target="#modal-activate-user"
                            onclick="setActivateUser(${position})">
                            <em class="icon ni ni-check"></em></button>
                            `;
                        }
                    }
                    else if (item.type == "guest" && item.status == "activate") {
                        status = `<span class="badge badge-pill badge-info">Terverifikasi</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-danger"
                        data-toggle="modal" data-target="#modal-delete-user"
                        onclick="setRemoveUser(${position})">
                        <em class="icon ni ni-trash"></em></button>
                        `;
                    }
                    else if (item.type == "guest" && item.status == "request activate") {
                        status = `<span class="badge badge-pill badge-warning">Permintaan Berlangganan</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-success"
                        data-toggle="modal" data-target="#modal-instalation-user"
                        onclick="setInstalationUser(${position})">
                        <em class="icon ni ni-check"></em></button>
                        <button class="btn btn-icon btn-sm btn-success"
                        data-toggle="modal" data-target="#modal-plot-user"
                        onclick="setPlotUser(${position})">
                        <em class="icon ni ni-user-check"></em></button>
                        <button class="btn btn-icon btn-sm btn-danger"
                        data-toggle="modal" data-target="#modal-delete-user"
                        onclick="setRemoveUser(${position})">
                        <em class="icon ni ni-trash"></em></button>
                        `;
                    } else if (item.type == "guest" && (item.status == "not activate yet" || item.status == "banned")) {
                        status = `<span class="badge badge-pill badge-warning">Biasa</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})">
                        <em class="icon ni ni-eye"></em></button>
                        <button class="btn btn-icon btn-sm btn-danger"
                        data-toggle="modal" data-target="#modal-delete-user"
                        onclick="setRemoveUser(${position})">
                        <em class="icon ni ni-trash"></em></button>
                        `;
                    }

                    return `
                    <tr>
                        <th scope="row">${(position + 1)}</th>
                        <td>${item.full_name}</td>
                        <td>${item.phone_number}</td>
                        <td>${status}</td>
                        <td>${(item.status == "activate") ? global.date(item.created_at) : "-"}</td>
                        <td>${action}</td>
                    </tr>
                    `;
                }).join('')}
            `);
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}