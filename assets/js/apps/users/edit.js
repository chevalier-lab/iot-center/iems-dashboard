"use strict";

var phone_number = "";
var user = null;

!function (NioApp, $) {
  "use strict";

  phone_number = $("#selected-user-phone_number").val();

  loadDetail()

  $('#user-new-phone_number').mask('(999) 999-999-999?9');

  $("#btn-user-back").on("click",function() {
    $("#btn-user-back").attr("disabled", true);
    $("#btn-user-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
      window.history.back();
    }, 1000);
  });

  $("#btn-user-new").on("click",function() {
    $("#btn-user-new").attr("disabled", true);
    $("#btn-user-new").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateUser();
  });

}(NioApp, jQuery);

function loadDetail() {

  global.getRAW(global.base_url + "/services/users/detail?phone_number=" + phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {

        if (res.code == 200) {
            user = res.data;

              $("#user-new-full_name").val(user.full_name)
              $("#user-new-email").val(user.email)
              $("#user-new-password").val(user.password)
              $("#user-new-phone_number").val(user.phone_number)
              $("#user-new-type").val(user.type)

              checkIsUser(user.type)

              if (user.type == "user") loadInfo()
            
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadInfo() {

  global.getRAW(global.base_url + "/services/users/info?phone_number=" + phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {

        if (res.code == 200) {
            var info = res.data;

            $("#user-new-ssid_name").val(info.ssid_name)
            $("#user-new-ssid_password").val(info.ssid_password)
            
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function checkIsUser(type) {
  if (type == "user") $(".if-user").show();
  else $(".if-user").hide();
}

function doCreateUser() {
  var full_name = $("#user-new-full_name").val()
  var email = $("#user-new-email").val()
  var password = $("#user-new-password").val()
  var phone_number = $("#user-new-phone_number").val()
  var type = $("#user-new-type").val()
  var ssid_name = $("#user-new-ssid_name").val()
  var ssid_password = $("#user-new-ssid_password").val()
  var status = (type == "guest") ? "not activate yet" : "activate"

  phone_number = phone_number.replace("-", "");
  phone_number = phone_number.replace("-", "");
  phone_number = phone_number.replace("(", "");
  phone_number = phone_number.replace(")", "");
  phone_number = phone_number.replace(" ", "");

  var raw = global.raw({
    full_name: full_name,
    email: email,
    password: password,
    phone_number: phone_number,
    type: type,
    status: status,
    ssid_name: ssid_name,
    ssid_password: ssid_password
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/users/update?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-user-new").removeAttr("disabled");
      $("#btn-user-new").html(`
        <span>Simpan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        
        setTimeout(() => {
          window.history.back();
        }, 1000);

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}