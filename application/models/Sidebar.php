<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends CI_Model {
    // Admin
    public function administrator() {
        return array(
            // Dashboard
            array(
                "header" => "Dashboard",
                "items" => array(
                    array(
                        "icon" => "home",
                        "label" => "Beranda",
                        "uri" => base_url("index.php/views/home")
                    ),
                    array(
                        "icon" => "ticket-alt",
                        "label" => "Ticketing",
                        "uri" => base_url("index.php/views/ticketing")
                    )
                )
            ),
            // Pengguna
            array(
                "header" => "Pengguna",
                "items" => array(
                    array(
                        "icon" => "users",
                        "label" => "Kelola Pengguna",
                        "uri" => base_url("index.php/views/users")
                    )
                )
            ),
            // Perangkat
            array(
                "header" => "Perangkat",
                "items" => array(
                    array(
                        "icon" => "archive",
                        "label" => "Kelola Perangkat",
                        "uri" => base_url("index.php/views/devices")
                    ),
                    array(
                        "icon" => "bell",
                        "label" => "Notifikasi Perangkat",
                        "uri" => base_url("index.php/views/notification")
                    )
                )
            ),
            // Utilitas
            array(
                "header" => "Utilitas",
                "items" => array(
                    array(
                        "icon" => "list-round",
                        "label" => "Catatan Aktifitas",
                        "uri" => base_url("index.php/views/logs")
                    )
                )
            )
        );
    }

    // Customer Service
    public function customerService() {

    }

    // Operator
    public function operator() {

    }

    // User
    public function user() {

    }

    // Guest
    public function guest() {

    }
}