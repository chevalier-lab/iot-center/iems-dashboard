<!DOCTYPE html>
<html lang="zxx" class="js">

<?php
    $this->load->view('templates/meta/head', array(
        "title" => isset($title) ? $title : "Portal",
        "additional" => isset($additional) ? $additional : ""
    ));
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <?php 
                $this->load->view('templates/components/sidebar'); 
            ?>
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <?php 
                    $this->load->view('templates/components/header'); 
                ?>
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                            <?= isset($content) ? $content : ""; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <?php 
                    $this->load->view('templates/components/footer'); 
                ?>
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    
    <?php
        // Load Component
        if (isset($components)) {
            foreach ($components as $item) {
                $this->load->view($item);
            }
        }

        $this->load->view('templates/meta/footer', array(
            "additional" => isset($jsLibrary) ? $jsLibrary : ""
        ));
    ?>

</body>

</html>