<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@@page-discription">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url('/'); ?>images/favicon.png">
    <!-- Page Title  -->
    <title><?= isset($title) ? $title : 'IEMS'; ?> </title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url('/'); ?>assets/css/dashlite.css?ver=1.4.0">
    <link id="skin-default" rel="stylesheet" href="<?= base_url('/'); ?>assets/css/theme.css?ver=1.4.0">
    <!-- Additional -->
    <?= isset($additional) ? $additional : "" ?>
</head>