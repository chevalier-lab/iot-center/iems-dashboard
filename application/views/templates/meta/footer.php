<!-- CONSTANTA JS -->
<input type="hidden" value="<?= base_url('index.php'); ?>"
id="base_url">
<input type="hidden" value="<?= isset($csrf_token) ? $csrf_token : ''; ?>"
id="csrf_token">

<!-- JavaScript -->
<script src="<?= base_url('/'); ?>assets/js/bundle.js?ver=1.4.0"></script>
<script src="<?= base_url('/'); ?>assets/js/scripts.js?ver=1.4.0"></script>
<script src="<?= base_url('/'); ?>assets/js/apps/general.js"></script>

<?php
    if (isset($auth)) {
        ?>
        <script src="<?= base_url('/'); ?>assets/js/apps/global-auth.js"></script>
        <?php
    }
?>

<!-- Library -->
<?= isset($additional) ? $additional : ''; ?>