<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-activate-user">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Aktifkan Pengguna?</h5>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin mengaktifkan <strong id="modal-activate-user-full_name"></strong>?</p>
            </div>
            <div class="modal-footer bg-light">

                <div class="clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-user-activate"
                        class="btn btn-wider btn-success">
                            <span>Aktifkan</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>