<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-plot-user">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Ploting Operator Ke Pengguna?</h5>
            </div>
            <div class="modal-body">

                <form class="row" id="modal-plot-user-form">
                    <div class="col-sm-12 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="user-plot-operator">Nama Operator</label>
                            <div class="form-control-wrap">
                                <select class="form-control" 
                                id="user-plot-operator" placeholder="Nama Operator"></select>
                            </div>
                        </div>
                    </div>
                </form>

                <p>Apakah anda yakin ingin memploting operator ke <strong id="modal-plot-user-full_name"></strong>?</p>
            </div>
            <div class="modal-footer bg-light">

                <div class="clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-user-plot"
                        class="btn btn-wider btn-success">
                            <span>Plot</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>