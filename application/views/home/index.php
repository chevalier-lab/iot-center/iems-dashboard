<?php
    $jsDir = base_url().'/assets/js/apps/';
    $jsDirExample = base_url().'/assets/js/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Beranda - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."home/index.js'></script>
            <script src='".$jsDirExample."example-chart.js?ver=1.4.0'></script>
        ",
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Beranda</h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong id="welcome-profile">Andy Maulana Yusuf</strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-monitor-device"
                                class="btn btn-primary"><em class="icon ni ni-monitor"></em><span>Pantau Perangkat</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">
                
                <div class="col-xxl-3">
                    <div class="card card-bordered h-100">
                        <div class="card-inner">

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Total Data</h6>
                                    <p>Total data IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total data sampai hari ini"></em>
                                </div>
                            </div>
                            <div class="row" id="meta-total-data"></div>

                        </div>
                    </div><!-- .card -->
                </div><!-- .col -->

                <div class="col-md-6 col-xxl-4">
                    <div class="card card-bordered h-100">
                        <div class="card-inner border-bottom">
                            <div class="card-title-group">
                                <div class="card-title">
                                    <h6 class="title">Notifikasi</h6>
                                </div>
                                <div class="card-tools">
                                    <a href="html/subscription/tickets.html" class="link">Lihat Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-inner">
                            <div class="timeline">
                                <h6 class="timeline-head">November, 2019</h6>
                                <ul class="timeline-list">
                                    <li class="timeline-item">
                                        <div class="timeline-status bg-primary is-outline"></div>
                                        <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                        <div class="timeline-data">
                                            <h6 class="timeline-title">Submited KYC Application</h6>
                                            <div class="timeline-des">
                                                <p>Re-submitted KYC Application form.</p>
                                                <span class="time">09:30am</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="timeline-item">
                                        <div class="timeline-status bg-primary"></div>
                                        <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                        <div class="timeline-data">
                                            <h6 class="timeline-title">Submited KYC Application</h6>
                                            <div class="timeline-des">
                                                <p>Re-submitted KYC Application form.</p>
                                                <span class="time">09:30am</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="timeline-item">
                                        <div class="timeline-status bg-pink"></div>
                                        <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                        <div class="timeline-data">
                                            <h6 class="timeline-title">Submited KYC Application</h6>
                                            <div class="timeline-des">
                                                <p>Re-submitted KYC Application form.</p>
                                                <span class="time">09:30am</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- .card -->
                </div>

                <div class="col-md-6 col-xxl-4">
                    <div class="card card-bordered card-full">
                        <div class="card-inner border-bottom">
                            <div class="card-title-group">
                                <div class="card-title">
                                    <h6 class="title">Aktifitas Terakhir</h6>
                                </div>
                                <div class="card-tools">
                                    <ul class="card-tools-nav">
                                        <a href="html/subscription/tickets.html" class="link">Lihat Selengkapnya</a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="nk-activity" id="log-last">
                        </ul>
                    </div><!-- .card -->
                </div>
                
                <div class="col-xxl-3">
                    <div class="card card-bordered h-100">
                        <div class="card-inner">

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Lokasi Perangkat</h6>
                                    <p>Posisi dari perangkat baik yang aktif ataupun yang tidak aktif</p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Lokasi Perangkat"></em>
                                </div>
                            </div>
                            
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.2988673283958!2d107.62834001436796!3d-6.974022670225966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd6285c5b7da517%3A0x864485f26a388f95!2sTelkom%20University!5e0!3m2!1sen!2sid!4v1609376612596!5m2!1sen!2sid" height="450" frameborder="0" style="border:0; width: 100%" 
                                allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                            <hr>

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Total Perangkat</h6>
                                    <p>Total perangkat aktif ataupun yang tidak aktif</p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                </div>
                            </div>
                            <div class="nk-ck">
                                <canvas class="line-chart" id="solidLineChart"></canvas>
                            </div>

                        </div>

                    </div><!-- .card -->
                </div><!-- .col -->

            </div><!-- .row -->
        </div>
        '
    ));
?>