<?php
    $jsDir = base_url().'/assets/js/apps/';
    $compDir = 'devices/components/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Kelola Perangkat - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."devices/index.js'></script>
        ",
        "components" => array(
            $compDir . "modal-blocked-device",
            $compDir . "modal-delete-device",
            $compDir . "modal-activate-device"
        ),
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Kelola Perangkat</h3>
                    <div class="nk-block-des text-soft">
                    <p>Selamat Datang <strong id="welcome-profile"></strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-device-add"
                                class="btn btn-primary"><em class="icon ni ni-plus"></em><span>Perangkat Baru</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">
                
                <div class="col-xxl-3">
                    <div class="card card-bordered h-100">
                        <div class="card-inner">

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Total Data</h6>
                                    <p>Total data perangkat IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total data sampai hari ini"></em>
                                </div>
                            </div>
                            <div class="row" id="meta-total-data">
                            </div>

                        </div>
                    </div><!-- .card -->
                </div><!-- .col -->

                <div class="col-xxl-8">
                    <div class="card card-bordered card-preview">
                        <div class="card-inner">
                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Daftar Perangkat</h6>
                                    <p>Daftar data perangkat IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Daftar Perangkat"></em>
                                </div>
                            </div>

                            <ul class="nav nav-tabs mt-n3">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterDevices(``)">Semua</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterDevices(`kwh-1-phase`)">KWH 1 Fasa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterDevices(`kwh-3-phase`)">KWH 3 Fasa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterDevices(`sensors`)">Sensor</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterDevices(`pcb`)">PCB</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterDevices(`slca`)">SLCA</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active">

                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-search"></em>
                                        </div>
                                        <input type="search" class="form-control"
                                        placeholder="Cari berdasarkan nama perangkat, dan ID perangkat">
                                    </div>
                                </div>
                                    
                                <div class="table-responsive">
                                    <table class="table" id="list-devices">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nama Perangkat</th>
                                                <th scope="col">ID Perangkat</th>
                                                <th scope="col">Jenis Perangkat</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Tanggal Aktif</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </div>
        '
    ));
?>