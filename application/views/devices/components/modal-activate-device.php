<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-activate-device">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Aktifkan Perangkat?</h5>
            </div>
            <div class="modal-body">
                <form class="row" id="modal-activate-device-form">
                    <div class="col-sm-6 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="device-activate-phone_number">Nomor Telepon Pelanggan</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" 
                                id="device-activate-phone_number" placeholder="Nomor Telepon Pelanggan">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="device-activate-name">Nama Perangkat</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" 
                                id="device-activate-name" placeholder="Nama Perangkat">
                            </div>
                        </div>
                    </div>
                </form>

                <p>Apakah anda yakin ingin mengaktifkan <strong id="modal-activate-device-name"></strong>?</p>
            </div>
            <div class="modal-footer bg-light">

                <div class="clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-device-activate"
                        class="btn btn-wider btn-success">
                            <span>Aktifkan</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>