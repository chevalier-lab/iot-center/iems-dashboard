<?php
    $jsDir = base_url().'/assets/js/apps/';

    $this->load->view('templates/dashboard', array(
        "title" => "+ Perangkat Baru - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."devices/add.js'></script>
        ",
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title"><em class="icon ni ni-plus"></em><span>Perangkat Baru</span></h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong>Andy Maulana Yusuf</strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-device-back"
                                class="btn btn-primary"><em class="icon ni ni-arrow-left"></em><span>Kembali</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">
                
                <div class="col-xxl-3">
                    <div class="card card-bordered h-100">
                        <div class="card-inner">

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Form Perangkat Baru</h6>
                                    <p>Form Perangkat baru IEMS</p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Form Perangkat Baru"></em>
                                </div>
                            </div>
                            <form class="row" id="device-new-form">

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="device-new-name">Nama Perangkat</label>
                                        <div class="form-control-wrap">
                                            <div class="form-icon form-icon-right"">
                                                <em class="icon ni ni-edit"></em>
                                            </div>
                                            <input type="text" class="form-control" 
                                            id="device-new-name" placeholder="Nama Perangkat">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="device-new-type">Jenis Perangkat</label>
                                        <div class="form-control-wrap">
                                            <select id="device-new-type" class="form-control">
                                                <option value="kwh-1-phase">KWH 1 Fasa</option>
                                                <option value="kwh-3-phase">KWH 3 Fasa</option>
                                                <option value="sensors">Sensor</option>
                                                <option value="pcb">Panel Circuit Breaker (PCB)</option>
                                                <option value="slca">Single Load With Ampermeter (SLCA)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <div class="mt-4 clearfix">
                                <div class="form-group">
                                    <button
                                    type="button" 
                                    style="float:right"
                                    id="btn-device-new"
                                    class="btn btn-wider btn-primary">
                                        <span>Simpan</span>
                                        <em class="icon ni ni-arrow-right"></em>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div><!-- .card -->
                </div><!-- .col -->

            </div><!-- .row -->
        </div>
        '
    ));
?>