<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Views extends CI_Controller
{
    // Public Variable
    public $session;
    public $csrf_token, $auth;
    public $menu;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("sidebar");

        // Load Helper
        $this->session = new Session_helper();        

        // Generate Valid Tokenize
        $this->csrf_token = $this->tokenize->generate();
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_AUTH);
            switch ($this->auth->type) {
                case "administrator": $this->menu = $this->sidebar->administrator();
                break;
                case "customer service": $this->menu = $this->sidebar->customerService();
                break;
                case "operator": $this->menu = $this->sidebar->operator();
                break;
                case "user": $this->menu = $this->sidebar->user();
                break;
                case "guest": $this->menu = $this->sidebar->guest();
                break;
            }
        }
    }

    // ------------------------------ PORTAL
    
    // Index
    public function index()
    {
        if (isset($this->auth))
        $this->home();
        else
        $this->signIn();
    }

    // Sign In
    public function signIn()
    {
        // Check is already authentication
        if (isset($this->auth))
        redirect(base_url("index.php"));

        $this->load->view("portal/sign-in", array(
            "csrf_token" => $this->csrf_token
        ));
    }

    // Forgot Password
    public function forgotPassword()
    {
        // Check is already authentication
        if (isset($this->auth))
        redirect(base_url("index.php"));

        $this->load->view("portal/forgot-password", array(
            "csrf_token" => $this->csrf_token
        ));
    }

    // Set Atuh
    public function set_auth()
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";
        if (!empty($raw)) {
            $auth = json_decode($raw);
            if (isset($auth->token)) {
                $this->session->add_session(IEMS_AUTH, $auth);
            }
        } 
        redirect(base_url("index.php"));
    }

    // ------------------------------ DASHBOARD

    // Home
    public function home()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php"));
        
        $this->load->view("home/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'home',
            "menu" => $this->menu
        ));
    }

    // Devices
    public function devices()
    {
        // Check is already authentication
        // $this->indexAdmin();
        
        $this->load->view("devices/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'devices',
            "menu" => $this->menu
        ));
    }

    // Add Devices
    public function devicesAdd()
    {
        // Check is already authentication
        // $this->indexAdmin();

        $this->load->view("devices/add", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'devices',
            "menu" => $this->menu
        ));
    }

    // Detail Devices
    public function devicesDetail()
    {
        // Check is already authentication
        // $this->indexAdmin();

        $device_token = $this->input->post_get("uid", TRUE) ?: "";
        $device_name = $this->input->post_get("uname", TRUE) ?: "";
        $device_type = $this->input->post_get("utype", TRUE) ?: "";

        $this->load->view("devices/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'devices',
            'udata' => array(
                "device_token" => $device_token,
                "device_name" => $device_name,
                "device_type" => $device_type
            ),
            "menu" => $this->menu
        ));
    }

    // Users
    public function users()
    {
        // Check is already authentication
        // $this->indexAdminUser();

        $this->load->view("users/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu
        ));
    }

    // Add Users
    public function usersAdd()
    {
        // Check is already authentication
        // $this->indexAdminUser();

        $this->load->view("users/add", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu
        ));
    }

    // Edit Users
    public function usersEdit()
    {
        // Check is already authentication
        // $this->indexAdminUser();

        $phone_number = $this->input->post_get("uid", TRUE) ?: "";
        $full_name = $this->input->post_get("uname", TRUE) ?: "";
        
        $this->load->view("users/edit", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users', 
            'udata' => array(
                "phone_number" => $phone_number,
                "full_name" => $full_name
            ),
            "menu" => $this->menu
        ));
    }

    // Detail Users
    public function usersDetail()
    {
        // Check is already authentication
        // $this->indexAdminUser();

        $phone_number = $this->input->post_get("uid", TRUE) ?: "";
        $full_name = $this->input->post_get("uname", TRUE) ?: "";
        
        $this->load->view("users/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users', 
            'udata' => array(
                "phone_number" => $phone_number,
                "full_name" => $full_name
            ),
            "menu" => $this->menu
        ));
    }

    // Notifications
    public function notification()
    {
        // Check is already authentication
        // $this->indexAdminUser();

        $this->load->view("notification/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'notification'
        ));
    }

    public function ticketing()
    {
        $this->load->view("ticketing/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'ticketing',
            "menu" => $this->menu
        ));
    }

    // Profile
    public function profile()
    {
        // Check is already authentication
        // $this->indexSign();

        $this->load->view("profile/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'profile'
        ));
    }

    // Edit Profile
    public function profileEdit()
    {
        // Check is already authentication
        // $this->indexSign();

        $this->load->view("profile/edit", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'profile'
        ));
    }

    // Monitoring Data Device
    public function monitoring()
    {
        // Check is already authentication
        // $this->indexSign();

        $this->load->view("monitoring/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'monitoring',
            "menu" => $this->menu
        ));
    }

    // Analytics Consume Electricity
    public function analitycsConsumeElectricity()
    {
        // Check is already authentication
        // $this->indexSign();

        $this->load->view("analytics/consume-electricity", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'analytics'
        ));
    }

    // Analytics Duration On Off
    public function analitycsDurationOnOff()
    {
        // Check is already authentication
        // $this->indexSign();

        $this->load->view("analytics/duration-on-off", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'analytics'
        ));
    }

    // Analytics Audit Energy
    public function analitycsAuditEnergy()
    {
        // Check is already authentication
        // $this->indexSign();

        $this->load->view("analytics/audit-energy", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'analytics'
        ));
    }

}
