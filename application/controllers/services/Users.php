<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Filter Users
    public function filter()
    {   
        $type = $this->input->get("type") ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users?type=$type");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // Detail User
    public function detail()
    {
        $phone_number = $this->input->get("phone_number") ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/detail?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // Do Delete User
    public function delete()
    {
        $phone_number = $this->input->get("phone_number") ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/delete?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // My Instalation User
    public function my()
    {
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/operator/manage/customer/my");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // List Guest By User
    public function listGuestByUser()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/listGuestByUser?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // Do Create
    public function create()
    {
        $raw = $this->input->post_get("raw") ?: "";
        
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/administrator/manage/users/create");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    public function banned()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/banned?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function activate()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/activate?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function instalation()
    {
        $raw = $this->input->post_get("raw") ?: "";
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/operator/manage/customer/activate");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    // Do Update
    public function update()
    {
        $raw = $this->input->post_get("raw") ?: "";
        
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/administrator/manage/users/update");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    // Info
    public function info()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/info?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // Plot
    public function plotOperator()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $phone_number_operator = $this->input->get("phone_number_operator", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/plotCustomerToOperator?phone_number=$phone_number&phone_number_operator=$phone_number_operator");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

}
